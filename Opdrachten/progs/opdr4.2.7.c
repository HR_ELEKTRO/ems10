#include <msp430.h> 


/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop de watchdog timer

    P1OUT = 0x00; // Register resetten
    P1DIR = 0x41; // P1.6 en P1.0 als output instellen
    P1OUT = P1OUT | 0x40; // Rode led op P1.6 aan
    P1OUT = P1OUT | 0x01; // Groene led op P1.0 aan

    return 0;
}
