#include <msp430.h> 

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop watchdog timer
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
       
//  Voor het initialiseren van de USCI in UART mode volgen we de stappen die 
//  gegeven zijn in paragraaf 15.3.1 van de SP430x2xx Family User’s Guide.

//  The recommended USCI initialization or reconfiguration process is:
//  1. Set UCSWRST ( BIS.B #UCSWRST,&UCAxCTL1 )
    UCA0CTL1 |= UCSWRST;
//  2. Initialize all USCI registers with UCSWRST = 1 (including UCAxCTL1)
    UCA0CTL0 = 0;
    UCA0CTL1 |= UCSSEL_2;
    UCA0BR0 = 6;
    UCA0BR1 = 0;
    UCA0MCTL = UCBRF_8 | UCBRS_0 | UCOS16;
//  3. Configure ports.
//  Zodat je de pinnen TXD en RXD van USCI_A0 kunt gebruiken.
//  Deze informatie kun je vinden op pagina 43 van de MSP4302x53 datasheet.
    P1SEL |= BIT1 | BIT2;
    P1SEL2 |= BIT1 | BIT2;
//  4. Clear UCSWRST via software ( BIC.B #UCSWRST,&UCAxCTL1 )
    UCA0CTL1 &= ~UCSWRST;
//  5. Enable interrupts (optional) via UCAxRXIE and/or UCAxTXIE

    while(1)
    {
        // Vul hier (later) de code van opdracht 8.1.4 in. 
    }
    return 0;
}
