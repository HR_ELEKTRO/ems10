def is_schrikkeljaar(jaar):
    return (jaar % 4 == 0) ^ (jaar % 100 == 0) ^ (jaar % 400 == 0)

def aantal_dagen_in_jaar(jaar):
    return 365 + is_schrikkeljaar(jaar)

print(aantal_dagen_in_jaar(int(input('Van welk jaar wil je weten\nhoeveel dagen het heeft? '))))
