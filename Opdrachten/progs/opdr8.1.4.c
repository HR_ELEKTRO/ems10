#include <msp430.h> 

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop watchdog timer
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
       
//  The recommended USCI initialization or reconfiguration process is:
//  1. Set UCSWRST ( BIS.B #UCSWRST,&UCAxCTL1 )
    UCA0CTL1 |= UCSWRST;
//  2. Initialize all USCI registers with UCSWRST = 1 (including UCAxCTL1)
    UCA0CTL0 = 0;
    UCA0CTL1 |= UCSSEL_2;
    UCA0BR0 = 6;
    UCA0BR1 = 0;
    UCA0MCTL = UCBRF_8 | UCBRS_0 | UCOS16;
//  3. Configure ports.
    P1SEL |= BIT1 | BIT2;
    P1SEL2 |= BIT1 | BIT2;
//  4. Clear UCSWRST via software ( BIC.B #UCSWRST,&UCAxCTL1 )
    UCA0CTL1 &= ~UCSWRST;
//  5. Enable interrupts (optional) via UCAxRXIE and/or UCAxTXIE

    while(1)
    {
        // Wacht tot de bit UCA0RXIFG in het register IFG2 geset is:
        while ((IFG2 & UCA0RXIFG) == 0)
        {
            // Wacht tot karakter ontvangen is
        }
        // Maak de char-variabele c gelijk aan register UCA0RXBUF:
        char c = UCA0RXBUF;
        // Wacht tot de bit UCA0TXIFG in het register IFG2 geset is:
        while ((IFG2 & UCA0TXIFG) == 0)
        {
            // Wacht tot karakter verstuurd kan worden
        }
        // Maak het register UCA0TXBUF gelijk aan de variabele c:
        UCA0TXBUF = c;
    }
    return 0;
}
