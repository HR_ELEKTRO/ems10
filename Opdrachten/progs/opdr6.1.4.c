#include <msp430.h> 

void zet_led_uit(int lednummer)
{
    P2OUT &= ~(1<<(lednummer - 1));
}

void zet_led_aan(int lednummer)
{
    P2OUT |= 1<<(lednummer - 1);
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr(void)
{
    typedef enum {opslot, dicht, open} Toestand;
    static Toestand toestand = opslot;

    switch (toestand)
    {
    case opslot:
        if ((P1IFG & 1<<0) != 0)
        {
            P1IFG &= ~(1<<0); // clear interrupt flag
            zet_led_uit(1);
            zet_led_aan(3);
            toestand = dicht;
        }
        if ((P1IFG & 1<<1) != 0)
        {
            P1IFG &= ~(1<<1); // clear interrupt flag
        }
        break;
    case dicht:
        if ((P1IFG & 1<<0) != 0)
        {
            P1IFG &= ~(1<<0); // clear interrupt flag
            zet_led_uit(3);
            zet_led_aan(1);
            toestand = opslot;
        }
        if ((P1IFG & 1<<1) != 0)
        {
            P1IFG &= ~(1<<1); // clear interrupt flag
            zet_led_uit(3);
            zet_led_aan(2);
            toestand = open;
        }
        break;
    case open:
        if ((P1IFG & 1<<0) != 0)
        {
            P1IFG &= ~(1<<0); // clear interrupt flag
        }
        if ((P1IFG & 1<<1) != 0)
        {
            P1IFG &= ~(1<<1); // clear interrupt flag
            zet_led_uit(2);
            zet_led_aan(3);
            toestand = dicht;
        }
        break;
    }
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
    if (CALBC1_1MHZ != 0xFF)
    {
        DCOCTL = 0; // Select lowest DCOx and MODx settings
        BCSCTL1 = CALBC1_1MHZ; // Set range
        DCOCTL = CALDCO_1MHZ; // Set DCO step + modulation*/

        P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
        P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag
        zet_led_aan(1);

        P1DIR &= ~(1<<0); // zet pin P1.0 op intput
        P1REN |= 1<<0; // zet interne weerstand aan bij pin P1.0
        P1OUT &= ~(1<<0); // selecteer pull down weerstand op pin P1.0.
        P1IE |= 1<<0; // zet interrupt aan bij pin P1.0
        P1IES &= ~(1<<0); // interrupt op opgaande flank
        P1IFG &= ~(1<<0); // clear interrupt flag

        P1DIR &= ~(1<<1); // zet pin P1.1 op intput
        P1REN |= 1<<1; // zet interne weerstand aan bij pin P1.1
        P1OUT |= 1<<1; // selecteer pull up weerstand op pin P1.1.
        P1IE |= 1<<1; // zet interrupt aan bij pin P1.1
        P1IES |= 1<<1; // interrupt op neergaande flank
        P1IFG &= ~(1<<1); // clear interrupt flag

        __enable_interrupt(); // zet interruptsysteem aan

        while (1)
        {
        }

    }
    return 0;
}
