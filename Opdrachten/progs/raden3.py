import random
geheim = random.randint(1, 99)
while True:
    getal = int(input('Raad een getal tussen 0 en 100: '))
    if getal == geheim:
        print('Je hebt het geraden!')
        break
    if getal > geheim:
        print('Te hoog')
    else:
        print('Te laag')
