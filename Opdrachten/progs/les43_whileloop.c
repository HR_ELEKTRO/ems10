#include <msp430.h>
#include <stdint.h>

int main()
{
    WDTCTL = WDTPW | WDTHOLD; // Stop de watchdog timer
    
    // Eerst alle code die 1 keer uitgevoerd moet worden
    // Zoals bijvoorbeeld het instellen van de I/O
    P1OUT = 0x00;
    P1DIR = 0x07;
    // en het declareren van variabelen
    volatile uint16_t wachttijd;

    while (1)
    {
        // Hier de rest van het programma
        // dat continue uitgevoerd moet worden
        P1OUT ^= 1<<1;

        wachttijd = 60000;
        while (wachttijd) {
            wachttijd--;
        }
    }
}
