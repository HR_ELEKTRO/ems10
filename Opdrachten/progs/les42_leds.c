#include <msp430.h> 

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;

    P1DIR = 0b00000111;
    P1OUT = 0b00000000;
    P1OUT = P1OUT | 0b00000011;

    return 0;
}
