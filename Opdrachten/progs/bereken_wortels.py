import math

def bereken_wortels(a, b, c):
    # Vul hier je code in
    return None

wortels = bereken_wortels(int(input('Geef a: ')), int(input('Geef b: ')), int(input('Geef c: ')))
aantal_wortels = len(wortels)
if aantal_wortels == 0:
    print('Deze vierkantsvergelijking heeft geen reële wortels.')
elif aantal_wortels == 1:
    print('Deze vierkantsvergelijking heeft één reële wortel:')
    print(wortels[0])
else:
    print('Deze vierkantsvergelijking heeft twee reële wortels:')
    print(wortels[0], wortels[1])
