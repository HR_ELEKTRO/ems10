#include <msp430.h> 

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;    // stop watchdog timer

    P1DIR = 0b01000001; // P1.6 en P1.0 als output instellen
    P1OUT = 0b01000001; // Rode led op P1.6 en groene led op P1.0 aan

    return 0;
}
