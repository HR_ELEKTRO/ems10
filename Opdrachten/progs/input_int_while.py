def can_convert_str_to_type(str, type):
    try:
        type(str)
    except:
        return False
    return True

def input_int(prompt, min, max):
    i = input(prompt)
    while not can_convert_str_to_type(i, int) or int(i) < min or int(i) > max:
        i = input(prompt)
    return int(i)

r1 = input_int('Geef het eerste toetsresultaat: ', 1, 10)
r2 = input_int('Geef het tweede toetsresultaat: ', 1, 10)
print('Het gemiddelde is:', (r1 + r2) / 2)

