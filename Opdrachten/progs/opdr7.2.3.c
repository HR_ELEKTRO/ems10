#include <msp430.h>

void test1(int parameter)
{
    parameter = 0;
}

void test2(int parameter_array[])
{
    parameter_array[0] = 0;
    parameter_array[1] = 0;
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
    int argument = 1;
    test1(argument);
    int argument_array[] = {2, 3};
    test2(argument_array);
    return 0;
}
