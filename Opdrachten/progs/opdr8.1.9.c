#include <msp430.h> 

// volatile globale variabele omdat deze variabele in twee isrs gebruikt wordt
volatile char karakter;

#pragma vector = USCIAB0TX_VECTOR
__interrupt void UART_TX_isr(void)
{
    UCA0TXBUF = karakter;
    IE2 &= ~UCA0TXIE; // disable TXD interrupt
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void UART_RX_isr(void)
{
    karakter = UCA0RXBUF;
    IE2 |= UCA0TXIE; // enable TXD interrupt
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop watchdog timer
    // DCO = 1 MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ; // Set range
    DCOCTL = CALDCO_1MHZ; // Set DCO step + modulation
    // We volgen de instructies uit de Family User's Guide paragraaf 15.3.1.
    // 1. Set UCSWRST
    UCA0CTL1 |= UCSWRST;
    // 2. Initialize all USCI registers with UCSWRST = 1 (including UCAxCTL1)
    // Parity disabled, LSB first, 8-bit data, One stop bit, UART mode, Asynchronous mode
    UCA0CTL0 = 0;
    // USCI clock source select = SMCLK
    UCA0CTL1 = UCSSEL_2 | UCSWRST;
    // Baudrate = 9600 zie paragraaf 15.3.13 in Family User's Guide.
    // Oversampling mode enabled
    UCA0BR0 = 6;
    UCA0BR1 = 0;
    UCA0MCTL = UCBRS_0 | UCBRF_8 | UCOS16;
    // 3. Configure ports.
    P1SEL |= BIT2 | BIT1;
    P1SEL2 |= BIT2 | BIT1;
    // 4. Clear UCSWRST via software
    UCA0CTL1 &= ~UCSWRST;
    // 5. Enable interrupts (optional) via UCAxRXIE and/or UCAxTXIE
    IE2 |= UCA0RXIE; // enable RXD interrupt

    __enable_interrupt();
    __low_power_mode_3();
    return 0;
}
