#include <msp430.h> 

void zet_led_aan(int lednummer)
{
    // vul hier je code in
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
    P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag

    while (1)
    {
        __delay_cycles(1100000); // wacht ongeveer een seconde
        zet_led_uit(3);
        zet_led_aan(1);
        __delay_cycles(1100000); // wacht ongeveer een seconde
        zet_led_uit(1);
        zet_led_aan(2);
        __delay_cycles(1100000); // wacht ongeveer een seconde
        zet_led_uit(2);
        zet_led_aan(3);
    }

    return 0;
}