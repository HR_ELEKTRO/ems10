#include <msp430.h>

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop de watchdog timer
    if (CALBC1_1MHZ == 0xFF)
    {
        while(1); // Doe niets
    }
    DCOCTL = 0; // Klokfrequentie = 1 MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag

    while (1)
    {
        __delay_cycles(2000000);
        P2OUT ^= 1<<0;  // inverteer pin P2.0
    }

    return 0;
}
