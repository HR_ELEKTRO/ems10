# © 2018 Hogeschool Rotterdam
# benader cos(x) met de volgende formule cos(x) = 1 - x^2/2! + x^4/4! - x^6/6! + ...
# in deze formule betekent x^4: x tot de macht 4. Dat werkt niet in Python.
# in deze formule betekent 4!: 4 faculteit. Dat werkt niet in Python.
# stap 0; formule herschrijven cos(x) = x^0/0! - x^2/2! + x^4/4! - x^6/6! + ...
# waarbij x de hoek in radialen is
# aantal termen is een variabele die moet worden ingelezen

# stap  1: lees x (floating point getal waarvan de cos bepaald moet worden)
# stap  2: lees aantal termen in (geheel getal > 0)
# stap  3: bepaal nummer voor elke term: 0, 1, 2, 3, ..., aantal_termen-1
# stap  4: bepaal constante voor elke term: 0, 2, 4, 6, ...
# stap  5: bepaal teller van elke term: x^0, x^2, x^4, x^6, ...
# stap  6: bepaal noemer van elke term: 0!, 2!, 4!, 6!, ...
# stap  7: bepaal teken van elke term: +1, -1, +1, -1, ...
# stap  8: bepaal elke term: +1*x^0/0!, -1*x^2/2!, +1*x^4/4!, -1*x^6/6!, ...
# stap  9: tel de termen bij elkaar op
# stap 10: bereken term 0 niet in for-lus

x = float(input('Geef de waarde van x: '))
aantal_termen = int(input('Geef het aantal termen: '))
while aantal_termen <= 0:
    aantal_termen = int(input('Geef het aantal termen (een integer ≥ 0): '))

term_teller = 1.0
term_noemer = 1
term_teken = 1
cos = 1.0
for term_nummer in range(1, aantal_termen):
    term_constante = term_nummer * 2
    term_teller = term_teller * x * x
    term_noemer = term_noemer * (term_constante - 1) * term_constante
    term_teken = term_teken * -1
    term = term_teken * term_teller / term_noemer
    cos = cos + term

print('cos(' + str(x) + ') benaderd met', aantal_termen, 'termen =', cos)
