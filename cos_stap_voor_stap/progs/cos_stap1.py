# © 2018 Hogeschool Rotterdam
# benader cos(x) met de volgende formule cos(x) = 1 - x^2/2! + x^4/4! - x^6/6! + ...
# in deze formule betekent x^4: x tot de macht 4. Dat werkt niet in Python.
# in deze formule betekent 4!: 4 faculteit. Dat werkt niet in Python.
# stap 0; formule herschrijven cos(x) = x^0/0! - x^2/2! + x^4/4! - x^6/6! + ...
# waarbij x de hoek in radialen is
# aantal termen is een variabele die moet worden ingelezen

# stap 1: lees x (floating point getal waarvan de cos bepaald moet worden)

x = float(input('Geef de waarde van x: '))

print('cos(' + str(x) + ') =', 0.0) # dit is nog niet het goede antwoord!

