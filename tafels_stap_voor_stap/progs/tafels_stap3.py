# © 2018 Hogeschool Rotterdam
# Dit programma leest een geheel getal 0 < n < 10 en drukt
# vervolgens de tafels van 1 tot en met n naast elkaar af

n = int(input('Geef de waarde van n [1..9]: '))
while n < 1 or n > 9:
    n = int(input('Geef de waarde van n (minimaal 1 en maximaal 9): '))

for tafel in range(1, n + 1):
    print(' 1 x', tafel, '= ', 1 * tafel, end=' ');
print();

