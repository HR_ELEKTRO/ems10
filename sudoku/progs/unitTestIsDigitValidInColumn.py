def isDigitValidInColumn(m, d, c):
    return True

def printTestResult(testNumber, testResult):
    if testResult:
        print('Test ' + str(testNumber) + ' is succesvol uitgevoerd.')
    else:
        print('Test ' + str(testNumber) + ' is NIET succesvol uitgevoerd.')

puzzle = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]

printTestResult(1, isDigitValidInColumn(puzzle, 6, 0))
printTestResult(2, not isDigitValidInColumn(puzzle, 6, 1))
printTestResult(3, isDigitValidInColumn(puzzle, 2, 3))
printTestResult(4, not isDigitValidInColumn(puzzle, 2, 4))
printTestResult(5, isDigitValidInColumn(puzzle, 9, 7))
printTestResult(6, not isDigitValidInColumn(puzzle, 9, 8))
