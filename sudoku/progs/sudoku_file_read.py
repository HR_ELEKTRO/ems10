class RowError(Exception):
    pass

class ColumnError(Exception):
    pass

def read_puzzle(filename):
    with open(filename) as text_file:
        puzzle = [[int(d) for d in row.split()] for row in text_file]
    if len(puzzle) != 9:
        raise RowError
    for r in range(9):
        if len(puzzle[r]) != 9:
            raise ColumnError
    return puzzle
   
def testread_puzzle(filename):
    try:
        puzzle = read_puzzle(filename)
        return puzzle
    except FileNotFoundError:
        print('Error: file niet gevonden.')
    except ValueError:
        print('Error: file bevat niet alleen getallen.')
    except RowError:
        print('Error: file bevat geen 9 rijen.')
    except ColumnError:
        print('Error: file bevat geen 9 kolommen.')

puzzle = testread_puzzle('sudoku_invalid1.txt')
print(puzzle)
puzzle = testread_puzzle('sudoku_invalid2.txt')
print(puzzle)
puzzle = testread_puzzle('sudoku_invalid3.txt')
print(puzzle)
puzzle = testread_puzzle('sudoku_die_niet_bestaat.txt')
print(puzzle)
puzzle = testread_puzzle('sudoku_1.txt')
print(puzzle)
