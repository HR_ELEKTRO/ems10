def is_digit_valid_in_block(m, d, r, c):
    return True

def print_test_result(test_number, test_result):
    if test_result:
        print('Test', test_number, 'is succesvol uitgevoerd.')
    else:
        print('Test', test_number, 'is Niet succesvol uitgevoerd.')

puzzle = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]

print_test_result(1, not is_digit_valid_in_block(puzzle, 6, 6, 6))
print_test_result(2, not is_digit_valid_in_block(puzzle, 6, 7, 7))
print_test_result(3, not is_digit_valid_in_block(puzzle, 6, 6, 8))
print_test_result(4, is_digit_valid_in_block(puzzle, 3, 6, 3))
print_test_result(5, is_digit_valid_in_block(puzzle, 3, 7, 4))
print_test_result(6, is_digit_valid_in_block(puzzle, 3, 6, 5))
