import copy

def printTestResult(testNumber, testResult):
    if testResult:
        print('Test ' + str(testNumber) + ' is succesvol uitgevoerd.')
    else:
        print('Test ' + str(testNumber) + ' is NIET succesvol uitgevoerd.')

puzzle1 = [
    [8, 6, 3, 9, 2, 5, 7, 4, 1],
    [4, 1, 2, 7, 8, 6, 3, 5, 9],
    [7, 5, 9, 4, 1, 3, 2, 8, 6],
    [9, 7, 1, 2, 6, 4, 8, 3, 5],
    [3, 4, 6, 8, 5, 7, 9, 1, 2],
    [2, 8, 5, 3, 9, 1, 4, 6, 7],
    [1, 9, 8, 6, 3, 2, 5, 7, 4],
    [5, 2, 4, 1, 7, 8, 6, 9, 3],
    [6, 3, 7, 5, 4, 9, 1, 2, 8]
]

puzzle2 = copy.deepcopy(puzzle1)
printTestResult(1, puzzle1 == puzzle2)
printTestResult(2, not puzzle1 is puzzle2)
