def is_digit_valid_in_row(m, d, r):
    for c in range(9):
        if m[r][c] == d:
            return False
    return True

def is_digit_valid_in_column(m, d, c):
    for r in range(9):
        if m[r][c] == d:
            return False
    return True

def is_digit_valid_in_block(m, d, r, c):
    rb = r - r % 3
    cb = c - c % 3
    for r in range(rb, rb + 3):
        for c in range(cb, cb + 3):
            if m[r][c] == d:
                return False
    return True

def is_valid_move(m, d, r, c):
    return is_digit_valid_in_row(m, d, r) and is_digit_valid_in_column(m, d, c) and is_digit_valid_in_block(m, d, r, c)

def print_test_result(test_number, test_result):
    if test_result:
        print('Test', test_number, 'is succesvol uitgevoerd.')
    else:
        print('Test', test_number, 'is Niet succesvol uitgevoerd.')

puzzle = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]

print_test_result(1, not is_valid_move(puzzle, 6, 0, 2))
print_test_result(2, is_valid_move(puzzle, 6, 2, 5))
print_test_result(3, not is_valid_move(puzzle, 4, 3, 0))
print_test_result(4, not is_valid_move(puzzle, 4, 4, 0))
print_test_result(5, not is_valid_move(puzzle, 4, 5, 0))
print_test_result(6, is_valid_move(puzzle, 9, 7, 7))

