#include <msp430.h>
#include <stdbool.h>
#include <stdint.h>

const int8_t SW0 = 1 << 0;
const int8_t SW1 = 1 << 1;
const int8_t ROOD = 1 << 0;
const int8_t GROEN = 1 << 1;
const int8_t BLAUW = 1 << 2;

bool knop_0_wordt_ingedrukt(void) {
    static bool was_ingedrukt = false;
    bool is_ingedrukt = (P1IN & SW0) != 0;
    bool wordt_ingedrukt = !was_ingedrukt && is_ingedrukt;
    was_ingedrukt = is_ingedrukt;
    return wordt_ingedrukt;
}

bool knop_1_wordt_ingedrukt(void) {
    static bool was_ingedrukt = false;
    bool is_ingedrukt = (P1IN & SW1) == 0;
    bool wordt_ingedrukt = !was_ingedrukt && is_ingedrukt;
    was_ingedrukt = is_ingedrukt;
    return wordt_ingedrukt;
}

// functies led_uit en led_aan kunnen gebruikt worden voor leds op P2.x
void led_uit(int8_t masker)
{
    P2OUT &= ~masker;
}
void led_aan(int8_t masker)
{
    P2OUT |= masker;
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    P2DIR |= BLAUW | GROEN | ROOD; // zet pins P2.2, P2.1 en P2.0 op output
    led_uit(BLAUW | GROEN);
    led_aan(ROOD);

    P1DIR &= ~(SW1 | SW0); // zet pin P1.0 en P1.1 op input
    P1REN |= SW1 | SW0; // zet interne weerstand aan bij pin P1.0 en P1.1
    P1OUT &= ~SW0; // selecteer pull down weerstand op pin P1.0
    P1OUT |= SW1; // selecteer pull up weerstand op pin P1.1

    typedef enum {opslot, dicht, open} Toestand;
    Toestand toestand = opslot;

    while (1)
    {
        switch (toestand)
        {
        case opslot:
            if (knop_0_wordt_ingedrukt())
            {
                led_uit(ROOD);
                led_aan(BLAUW);
                toestand = dicht;
            }
            break;
        case dicht:
            if (knop_0_wordt_ingedrukt())
            {
                led_uit(BLAUW);
                led_aan(ROOD);
                toestand = opslot;
            }
            if (knop_1_wordt_ingedrukt())
            {
                led_uit(BLAUW);
                led_aan(GROEN);
                toestand = open;
            }
            break;
        case open:
            if (knop_1_wordt_ingedrukt())
            {
                led_uit(GROEN);
                led_aan(BLAUW);
                toestand = dicht;
            }
            break;
        }
        __delay_cycles(11000); // 10 ms wachten @ 1.1 MHz (zorgt voor softwarematig ontdenderen van de knop)
    }
    return 0;
}
