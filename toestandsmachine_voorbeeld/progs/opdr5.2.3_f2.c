#include <msp430.h>
#include <stdbool.h>
#include <stdint.h>

const int8_t SW0 = 1 << 0;
const int8_t SW1 = 1 << 1;
const int8_t ROOD = 1 << 0;
const int8_t GROEN = 1 << 1;
const int8_t BLAUW = 1 << 2;

// functie knop_is_ingedrukt kan gebruikt worden voor knoppen op P1.x
typedef enum {PULL_UP, PULL_DOWN} pull_up_or_pull_down;
bool knop_is_ingedrukt(int8_t masker, pull_up_or_pull_down p)
{
    return p == PULL_UP && (P1IN & masker) == 0 || p == PULL_DOWN && (P1IN & masker) != 0;
}

// functies led_uit en led_aan kunnen gebruikt worden voor leds op P2.x
void led_uit(int8_t masker)
{
    P2OUT &= ~masker;
}
void led_aan(int8_t masker)
{
    P2OUT |= masker;
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    P2DIR |= BLAUW | GROEN | ROOD; // zet pins P2.2, P2.1 en P2.0 op output
    led_uit(BLAUW | GROEN);
    led_aan(ROOD);

    P1DIR &= ~(SW1 | SW0); // zet pin P1.0 en P1.1 op input
    P1REN |= SW1 | SW0; // zet interne weerstand aan bij pin P1.0 en P1.1
    P1OUT &= ~SW0; // selecteer pull down weerstand op pin P1.0
    P1OUT |= SW1; // selecteer pull up weerstand op pin P1.1

    typedef enum {opslot, dicht, open} Toestand;
    Toestand toestand = opslot;

    bool knop0_was_ingedrukt = false;
    bool knop1_was_ingedrukt = false;

    while (1)
    {
        bool knop0_is_ingedrukt = knop_is_ingedrukt(SW0, PULL_DOWN);
        bool knop1_is_ingedrukt = knop_is_ingedrukt(SW1, PULL_UP);

        switch (toestand)
        {
        case opslot:
            if (!knop0_was_ingedrukt && knop0_is_ingedrukt)
            {
                led_uit(ROOD);
                led_aan(BLAUW);
                toestand = dicht;
            }
            break;
        case dicht:
            if (!knop0_was_ingedrukt && knop0_is_ingedrukt)
            {
                led_uit(BLAUW);
                led_aan(ROOD);
                toestand = opslot;
            }
            if (!knop1_was_ingedrukt && knop1_is_ingedrukt)
            {
                led_uit(BLAUW);
                led_aan(GROEN);
                toestand = open;
            }
            break;
        case open:
            if (!knop1_was_ingedrukt && knop1_is_ingedrukt)
            {
                led_uit(GROEN);
                led_aan(BLAUW);
                toestand = dicht;
            }
            break;
        }
        knop0_was_ingedrukt = knop0_is_ingedrukt;
        knop1_was_ingedrukt = knop1_is_ingedrukt;
        __delay_cycles(11000); // 10 ms wachten @ 1.1 MHz (zorgt voor softwarematig ontdenderen van de knop)
    }
    return 0;
}
