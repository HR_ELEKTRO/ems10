#include <msp430.h>
#include <stdbool.h>

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    P2DIR |= 1 << 0; // zet pin P2.0 op output
    P2OUT &= ~(1 << 0); // P2.0 = 0

    P1DIR &= ~(1 << 0); // zet pin P1.0 op input
    P1REN |= 1 << 0; // zet interne weerstand aan bij pin P1.0
    P1OUT &= ~(1 << 0); // selecteer pull down weerstand op pin P1.0

    bool knop0_was_ingedrukt = false;
    while (1)
    {
        bool knop0_is_ingedrukt = (P1IN & (1 << 0)) != 0;
        if (!knop0_was_ingedrukt && knop0_is_ingedrukt)
        {
            P2OUT ^= 1 << 0; // toggle P2.0
        }
        knop0_was_ingedrukt = knop0_is_ingedrukt;
        __delay_cycles(11000); // 10 ms wachten @ 1.1 MHz (zorgt voor softwarematig ontdenderen van de knop)
    }
    return 0;
}
