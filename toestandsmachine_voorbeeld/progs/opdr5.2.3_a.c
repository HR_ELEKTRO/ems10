#include <msp430.h>
#include <stdbool.h>

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    P2DIR |= 1 << 2 | 1 << 1 | 1 << 0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1 << 2 | 1 << 1); // P2.2 en P2.1 laag Blauw uit en Groen uit
    P2OUT |= 1 << 0; // P2.0 = 1 Rood aan

    P1DIR &= ~(1 << 1 | 1 << 0); // zet pin P1.1 en P1.0 op input
    P1REN |= 1 << 1 | 1 << 0; // zet interne weerstand aan bij pin P1.1 en P1.0
    P1OUT &= ~(1 << 0); // selecteer pull down weerstand op pin P1.0
    P1OUT |= 1 << 1; // selecteer pull up weerstand op pin P1.1

    int toestand = 0; // begin in toestand op slot

    bool knop0_was_ingedrukt = false;
    bool knop1_was_ingedrukt = false;

    while (1)
    {
        bool knop0_is_ingedrukt = (P1IN & (1 << 0)) != 0;
        bool knop1_is_ingedrukt = (P1IN & (1 << 1)) == 0;

        switch (toestand)
        {
        case 0: // op slot
            if (!knop0_was_ingedrukt && knop0_is_ingedrukt)
            {
                P2OUT &= ~(1 << 0); // P2.0 = 0  Rood uit
                P2OUT |= 1 << 2; // P2.2 = 1 Blauw aan
                toestand = 1; // dicht
            }
            break;
        case 1: // dicht
            if (!knop0_was_ingedrukt && knop0_is_ingedrukt)
            {
                P2OUT &= ~(1 << 2); // P2.2 = 0 Blauw uit
                P2OUT |= 1 << 0; // P2.0 = 1 Rood aan
                toestand = 0; // op slot
            }
            if (!knop1_was_ingedrukt && knop1_is_ingedrukt)
            {
                P2OUT &= ~(1 << 2); // P2.2 = 0 Blauw uit
                P2OUT |= 1 << 1; // P2.1 = 1 Groen aan
                toestand = 2; // open
            }
            break;
        case 2: // open
            if (!knop1_was_ingedrukt && !knop1_is_ingedrukt)
            {
                P2OUT &= ~(1 <<1); // P2.1 = 0 Groen uit
                P2OUT |= 1 << 2; // P2.2 = 1 Blauw aan
                toestand = 1; // dicht
            }
            break;
        }
        knop0_was_ingedrukt = knop0_is_ingedrukt;
        knop1_was_ingedrukt = knop1_is_ingedrukt;
        __delay_cycles(11000); // 10 ms wachten @ 1.1 MHz (zorgt voor softwarematig ontdenderen van de knop)
    }
    return 0;
}
