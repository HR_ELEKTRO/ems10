#include <msp430.h>
#include <stdbool.h>
#include <stdint.h>

const int8_t SW0 = 1 << 0;
const int8_t SW1 = 1 << 1;
const int8_t ROOD = 1 << 0;
const int8_t GROEN = 1 << 1;
const int8_t BLAUW = 1 << 2;

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    P2DIR |= BLAUW | GROEN | ROOD; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(BLAUW | GROEN); // Blauw uit en Groen uit
    P2OUT |= ROOD; // Rood aan

    P1DIR &= ~(SW1 | SW0); // zet pin P1.0 en P1.1 op input
    P1REN |= SW1 | SW0; // zet interne weerstand aan bij pin P1.0 en P1.1
    P1OUT &= ~SW0; // selecteer pull down weerstand op pin P1.0
    P1OUT |= SW1; // selecteer pull up weerstand op pin P1.1

    typedef enum {opslot, dicht, open} Toestand;
    Toestand toestand = opslot;

    bool knop0_was_ingedrukt = false;
    bool knop1_was_ingedrukt = false;

    while (1)
    {
        bool knop0_is_ingedrukt = (P1IN & SW0) != 0;
        bool knop1_is_ingedrukt = (P1IN & SW1) == 0;

        switch (toestand)
        {
        case opslot:
            if (!knop0_was_ingedrukt && knop0_is_ingedrukt)
            {
                P2OUT &= ~ROOD; // Rood uit
                P2OUT |= BLAUW; // Blauw aan
                toestand = dicht;
            }
            break;
        case dicht:
            if (!knop0_was_ingedrukt && knop0_is_ingedrukt)
            {
                P2OUT &= ~BLAUW; // Blauw uit
                P2OUT |= ROOD; // Rood aan
                toestand = opslot;
            }
            if (!knop1_was_ingedrukt && knop1_is_ingedrukt)
            {
                P2OUT &= ~BLAUW; // Blauw uit
                P2OUT |= GROEN; // Groen aan
                toestand = open;
            }
            break;
        case open:
            if (!knop1_was_ingedrukt && knop1_is_ingedrukt)
            {
                P2OUT &= ~GROEN; // Groen uit
                P2OUT |= BLAUW; // Blauw aan
                toestand = dicht;
            }
            break;
        }
        knop0_was_ingedrukt = knop0_is_ingedrukt;
        knop1_was_ingedrukt = knop1_is_ingedrukt;
        __delay_cycles(11000); // 10 ms wachten @ 1.1 MHz (zorgt voor softwarematig ontdenderen van de knop)
    }
    return 0;
}
