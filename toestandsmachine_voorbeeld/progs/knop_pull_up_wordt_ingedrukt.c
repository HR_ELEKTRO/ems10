#include <msp430.h>
#include <stdbool.h>

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    P2DIR |= 1 << 0; // zet pin P2.0 op output
    P2OUT &= ~(1 << 0); // P2.0 = 0

    P1DIR &= ~(1 << 1); // zet pin P1.1 op input
    P1REN |= 1 << 1; // zet interne weerstand aan bij pin P1.1
    P1OUT |= 1 << 1; // selecteer pull up weerstand op pin P1.1

    bool knop1_was_ingedrukt = false;
    while (1)
    {
        bool knop1_is_ingedrukt = (P1IN & (1 << 1)) == 0;
        if (knop1_was_ingedrukt == false && knop1_is_ingedrukt == true)
        {
            P2OUT ^= 1 << 0; // toggle P2.0
        }
        knop1_was_ingedrukt = knop1_is_ingedrukt;
    }
    return 0;
}
